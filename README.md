# Improving pedestrian detection using motion-guided filtering #

This is the repository for the **Improving pedestrian detection using motion-guided filtering** project, coded in Matlab.

## Requires ##
- `Piotr's Matlab toolbox` Download the toolbox at 
https://pdollar.github.io/toolbox/ and add it to your matlab path.
- `openCV toolbox` for HOG+SVM pedestrian detector download the toolbox at 
https://opencv.org/downloads.html

## Dataset ##
- The dataset for this project can be found at: https://www.dropbox.com/sh/5b3shwhdn10jh7y/AADZdwslxbpG8rc3jgWq19fva?dl=0

## How to use ##
1. Download the dataset and rename it as 'dataset' the `\PedestrianDetection\dataset\`
2. The entrance of the project is the `main.m` file. To run the code, just indicates the operation and the detector.
```
Examples:
1. main(1, 1): use ACF detector to detect pedestrian on the original video
2. main(2, 1): use ACF detector to detect pedestrian on the filtered video
2. main(3, 1): merge the results of 1 and 2 to get the FINAL merged results for ACF detector.
```