function background = BG_initialization(video)
%%%%%
% Author: Yi Wang

% Calculate the median background with a certain number of frame

% input:
%   video, string:
%   the video name

% returns:
%   background, matrix:
%   the estimated background
%%%%%
    videoPath = fullfile('..', 'dataset', 'input', video);
    frames = dir(fullfile(videoPath, '*.jpg'));

    counter = 0;
    
    % use the first 25 frames with a setp length of 10
    for frameID = 1 : 10 : min(241, length(frames))
        counter = counter + 1;
        background_4d(:, :, :, counter) = imread(fullfile(videoPath,...
                                                 frames(frameID).name));
    end

    background = median(background_4d, 4);
    background = double(background);
end