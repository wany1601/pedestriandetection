function filtered_image = filter_image(image, sigmas, interpolate)
%%%%%
% To filter a image.

% input:
%   image, matrix:
%   the image that needs to be filtered

%   sigmas, matrix:
%   the sigmas for all pixels

% output:
%   filtered_image, matrix:
%   the filtered image
%%%%%
    image = mean (image, 3) ; % work on grayscale
    max_sigma = max(1, max(max(sigmas)));
    num_filtered_images = min(2 + ceil(max_sigma / 2), 25);
    sigma_values = linspace(0, max_sigma, num_filtered_images);
    filtered_images = zeros([(size(image)), num_filtered_images]);
    filtered_images(:, :, 1) = image;
    
    for i = 2 : num_filtered_images;
        delta_sigma = sqrt(sigma_values(i) ^2 - sigma_values(i - 1) ^2);
        filter_size = max(3, 1 + 2 * ceil (delta_sigma * 3.0));
        filter = fspecial('gaussian', filter_size, delta_sigma);
        filtered_images(:, :, i) = imfilter(filtered_images(:, :, i - 1), filter, 'same');
    end
    
    x = repmat((1 : size(image, 1))', 1, size(image, 2));
    y = repmat((1 : size(image, 2)), size(image, 1), 1);
    
    if interpolate
        index = 1 + sigmas * (num_filtered_images - 1) / max_sigma;
        index_inf = floor(index);
        index_sup = 1 + index_inf;
        weight_inf = index_sup - index;
        weight_sup = 1 - weight_inf;
        index_inf = max(1, min(num_filtered_images, index_inf));
        index_sup = max(1, min(num_filtered_images, index_sup));
        filtered_image_inf = filtered_images(sub2ind(size(filtered_images), x, y, index_inf));
        filtered_image_sup = filtered_images(sub2ind(size(filtered_images), x, y, index_sup));
        filtered_image = filtered_image_inf .* weight_inf + filtered_image_sup .* weight_sup;
    else
        index = 1 + sigmas * (num_filtered_images - 1) / max_sigma;
        index = max(1, min(num_filtered_images, round(index)));
        filtered_image = filtered_images(sub2ind(size(filtered_images), x, y, index));
    end
end