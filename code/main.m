function main(operation_id, detector_id)
%%%%%
% Author: Yi Wang
% Call all the sub functions in a pipeline for pedestrian detection
% includs (1) predestrian detection; (3) evaluation

% input:
%   operation_id, int:
%   the operation you want to take:  1. detecte on original video
%                                    2. detecte on filtered video
%                                    3. merge the results of 1 and 2.
%                                    4. evaluation

%   detector_id, int:
%   the detector id:  1. ACF
%                     2. HOG+SVM
%%%%%
    % constant
    PROJECT_PATH = strrep(pwd, 'code', '');     % project path

    input_path = fullfile(PROJECT_PATH, 'dataset', 'input');
    videos = dir(input_path); videos = videos(3 : end);
    switch operation_id
        case 4
            evaluation(BB_path, GT_path_selected_frames);
        otherwise
            for video_id = 1 : length(videos)
                if videos(video_id).isdir
                    video = videos(video_id).name;
                    switch operation_id
                        case 1  % detect on the original video
                            pedestrian_detection_original(detector_id, video);
                        case 2  % detect on the filtered video
                            pedestrian_detection_filtered(detector_id, video);
                        case 3  % merge the results of 1 and 2
                            pedestrian_detection_ori_fil_merged(detector_id, video);
                    end
                end
            end
    end 
end

