function pedestrian_detection_filtered(detector_id, video)
%%%%%
% Author: Yi Wang

% Pedestiran detection on filtered images. This function will save bounding
% boxes and output image for each frame in the video.

% input:
%   detector_id, int:
%   the detecter id. 1. ACF
%                    2. HOG+SVM

%   video, string:
%   the video to detect
%%%%%
    % constants
    SIGMA_RATIO = 5;        % sigma = image_height / SIGMA_RATIO
    NMS_THRESHOLD = 0.5;    % the threshold for NMS
    ACF_THRESHOLD = -5;     % the threshold for ACF detector
    SLOPE = 1.25;           % the slope of the line (MHI-pdf(probability))
    PROJECT_PATH = strrep(pwd, 'code', '');     % project path
    MODE = 'filtered';      % the operation mode 'original', 'filtered', 'merged'
    
    % load the pre-calculated parameter: resize ratio
    load(fullfile(PROJECT_PATH, 'parameters', 'resize_ratio_all'));
    
    % each line in the file represent a detector, while each column
    % represent a video
    if video > size(resize_ratio_all, 2)
        resize_ratio_video = 1;
    else
        resize_ratio_video = resize_ratio_all(str2double(detector_id), str2double(video));
    end
    
    % load the min and max score for each detector. Frist line: min; Second
    % line: max.
    load(fullfile(PROJECT_PATH, 'parameters', 'min_max_score'));
    score_min = min_max_score(1, str2double(video), detector_id);
    score_max = min_max_score(2, str2double(video), detector_id);

    input_video_path = fullfile(PROJECT_PATH, 'dataset', 'input', video);
    input_frames = dir(fullfile(input_video_path, '*jpg'));

    bbs_after_resize = [];
    
    % initialize the background, the MHI and the sigma_max
    BG = BG_initialization(video); BG = imresize(BG, resize_ratio_video);
    MHI = zeros(size(BG, 1), size(BG, 2));
    sigma_max = size(BG, 1) / SIGMA_RATIO;
    
    switch detector_id
        case 1
            detector_name = 'ACF';
            load('AcfCaltechDetector.mat'); %select between: 1. AcfInriaDetector 2. AcfCaltechDetector
            detector.opts.cascThr = ACF_THRESHOLD;     % set score threshold for the detector

            for input_id = 1 : length(input_frames)
                filename = input_frames(input_id).name;
                I_before_resize = imread(fullfile(input_video_path, input_frames(input_id).name));
                I_after_resize = imresize(I_before_resize, resize_ratio_video, 'nearest');
                
                % update the background and the MHI
                [BG, MHI] = BG_MHI_update(I_after_resize, bbs_after_resize, BG, MHI, score_min, score_max);
                
                % update the sigma
                sigmas = min(sigma_max, SLOPE * sigma_max * (1 - MHI));
                
                % filter the image
                I_filtered_after_resize = uint8(filter_image(I_after_resize, sigmas, 1));
                
                % detection
                bbs_after_resize = acfDetect(I_filtered_after_resize, detector);
                
                % resize back the bbs [x, y , h, w], keep the score.
                bbs_before_resize(:, 1 : 4) = round(bbs_after_resize(:, 1 : 4) / resize_ratio_video); 
                bbs_before_resize(:, 5) = bbs_after_resize(:, 5);
                
                % non maximum suppression
                bbs_before_resize = bbNms(bbs_before_resize, 'type', 'max', 'overlap', NMS_THRESHOLD);
                
                % save the results
                save_image(I_before_resize, bbs_before_resize, MODE, detector_name, video, filename, 0);
                save_BB(bbs_before_resize, MODE, detector_name, video, filename);

                clear bbs_before_resize
            end
            
        case 2
            detector_name = 'HOG+SVM';
            EXE_PATH = ['"', PROJECT_PATH, 'code\pedestrianDetector\HOG+SVM\HOG+SVM.exe"'];
            
            % these two pathes shold be premake before being called by the
            % detector
            BB_path = fullfile(PROJECT_PATH, 'output', 'BB', filesep);
            BB_video_path = fullfile(BB_path, MODE, detector_name, video);
            if ~exist(BB_video_path, 'dir'), mkdir(BB_video_path); end
            output_image_path = fullfile(PROJECT_PATH, 'output', 'image', filesep);
            output_image_video_path = fullfile(output_image_path, MODE, detector_name, video);
            if ~exist(output_image_video_path, 'dir'), mkdir(output_image_video_path); end

            for input_id = 1 : length(input_frames)
                % read and resize the image
                filename = input_frames(input_id).name;
                I_before_resize_path = fullfile(input_video_path, filename);
                I_before_resize = imread(I_before_resize_path);
                I_after_resize = imresize(I_before_resize, resize_ratio_video, 'nearest');
                
                % update the background and the MHI
                [BG, MHI] = BG_MHI_update(I_after_resize, bbs_after_resize, BG, MHI, score_min, score_max);
                
                % update the sigma
                sigmas = min(sigma_max, SLOPE * sigma_max * (1 - MHI));
                
                % filter the image and resize it back
                I_filtered_after_resize = filter_image(I_after_resize, sigmas, 1); I_filtered_after_resize = uint8(I_filtered_after_resize);
                I_filtered_before_resize = imresize(I_filtered_after_resize,  1 / resize_ratio_video, 'nearest');

                % save a tmp file of the filtered image for the detector
                filtered_input_path = ['image_HOGSVM_', video, '.jpg'];
                imwrite(I_filtered_before_resize, filtered_input_path, 'jpg');
                
                % call the opencv HOG+SVM pedestrian detector, the detector
                % detect pedestian without NMS
                system([EXE_PATH ' ' filtered_input_path ' ' num2str(resize_ratio_video) ' ' MODE ' ' video ' ' filename ' ' BB_path ' ' output_image_path]);
                clear bbs_after_resize
               
                % read the bounding boxes and apply NMS
                BB_before_NMS_path = fullfile(BB_video_path, [filename, '.txt']);
                bbs_before_resize = load(BB_before_NMS_path);
                bbs_before_resize = bbNms(bbs_before_resize, 'type','max', 'overlap', NMS_THRESHOLD);
                bbs_after_resize(:, 5) = bbs_before_resize(:, 5);
                
                I_before_resize = imread(I_before_resize_path);
                
                % save the results
                save_image(I_before_resize, bbs_before_resize, MODE, detector_name, video, filename, 0);
                save_BB(bbs_before_resize, MODE, detector_name, video, filename);
            end
            
            % delete the temporal '.jpg' file created 
            delete(filtered_input_path);
    end
end

function [BG_this_frame, MHI_this_frame] = BG_MHI_update(I_original,...
    bbs_last_frame, BG_last_frame, MHI_last_frame, score_min, score_max)
%%%%%
% Update the BG and MHI when a new frame comes.

% input:
%   I_original, matrix:
%   the original image

%   bbs_last_frame, matrix
%   the bounding box detected from the last frame: [x, y, h, w, score]

%   BG_last_frame, matrix
%   the background image estimated from the previous frames

%   MHI_last_frame
%   the MHI estimated from the previous frames

%   score_min
%   the minimum score given by the detector for the original video

%   score_max
%   the maximum score given by the detector for the original video

% returns:
%   BG_this_frame, martix:
%   the background image estimated with the new frame

%   MHI_this_frame
%   the MHI estimated with the new frame
%%%%%

    %constants
    ALPHA = 0.8;                % MHI updating ratio
    BETA_MAX = 0.016;           % maximum value of the background updating ratio
    W_DSM = 0.9;                      % the weight of DSM
    
    I = double(I_original);
    [h, w, ~] = size(I);
    
    % calculate the DSM
    dsm = bbs_2_dsm(bbs_last_frame, [h, w], score_min, score_max);

    % calculate delta
    delta = sqrt(sum((I - BG_last_frame) .^ 2, 3));
    delta = min(1, delta / 100);

    % update beta
    beta = BETA_MAX * (1 - W_DSM * dsm);
    
    % update MHI and background
    MHI_this_frame = max(delta, delta * ALPHA + MHI_last_frame * (1 - ALPHA));
    BG_this_frame = I .* repmat(beta, [1, 1, 3]) + BG_last_frame .* repmat(1 - beta, [1, 1, 3]);
end

function dsm = bbs_2_dsm(bbs, image_size, score_min, score_max)
%%%%%
% Calculate the DSM from the bounding boxes.
% input:
%   bbs, matrix
%   the bounding box: [x, y, h, w, score]

%   image_size, [1,2] vector:
%   the size of the image: [h, w]

%   score_min
%   the minimum score given by the detector for the original video

%   score_max
%   the maximum score given by the detector for the original video

% returns:
%   dsm, martix:
%   the detecting score map
%%%%%
    % constants
    SIGMA = 5;  % the standard diviation of the Gaussian filter
    
    dsm = zeros(image_size(1), image_size(2));
    
    if ~isempty(bbs)
        h_max = image_size(1); w_max = image_size(2);
        
        for row = 1 : size(bbs, 1)
            % load position from bounding boxes
            x = round(bbs(row, 1)); 
            y = round(bbs(row, 2)); 
            w = round(bbs(row, 3)); 
            h = round(bbs(row, 4)); 
            
            % transfer the postion to left, right, top and bottoms
            left = max(1, x); 
            right = min(w_max, x + w); 
            top = max(1, y); 
            bottom = min(h_max, y + h);
            
            % set scores for each pixel in DSM
            dsm(top : bottom, left : right) = ...
                max(dsm(top : bottom, left : right),...
                (bbs(row, 5) - score_min) / (score_max - score_min));
        end
        
        % Gaussian filter
        H = fspecial('gaussian', [SIGMA, SIGMA], SIGMA / 6); 
        dsm = imfilter(dsm, H, 'replicate');
    end
end