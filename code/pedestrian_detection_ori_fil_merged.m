function pedestrian_detection_ori_fil_merged(detector_id, video)
%%%%%
% Author: Yi Wang

% Merge the bounding boxes detected on the original video and the filtered
% video.

% input:
%   detector_id, int:
%   the detecter id. 1. ACF
%                    2. HOG+SVM

%   video, string:
%   the video to detect
%%%%%
    % constants:
    MERGE_THRESHOLD = 0.25; % the overlaping threshold for merging two bounding boxes
    MODE = 'merged';      % the operation mode 'original', 'filtered', 'merged'
    
    switch detector_id
        case 1
            detector_name = 'ACF';
        case 2
            detector_name = 'HOG+SVM';
    end

    input_video_path = fullfile('..', 'dataset', 'input', video);
    input_frames = dir(fullfile(input_video_path, '*jpg'));
    
    original_BB_path = fullfile('..', 'output', 'BB', 'original', [detector_name, '_NMS'], video);
    filtered_BB_path = fullfile('..', 'output', 'BB', 'filtered', [detector_name, '_NMS'], video);
    BB_files = dir(fullfile(original_BB_path, '*.txt'));    % the names are the same for both original and filtered
    
    for input_id = 1 : length(input_frames)
        filename = BB_files(input_id).name;
        
        % load bbs detected on the original video
        bbs_original = load(fullfile(original_BB_path, filename)); 

        % load bbs detected on the original video
        bbs_filtered = load(fullfile(filtered_BB_path, filename));
        
        % merge the bounding boxes
        if ~isempty(bbs_filtered) && ~isempty(bbs_original)
            overlapping_ratio = bbGt('compOas', round(bbs_filtered(:, 1 : 4)), bbs_original(:, 1 : 4));
            overlapping_ratio = max(overlapping_ratio, [], 2); %the largest overlapping area for each bb in the filtered resutls
            overlapping_ratio = overlapping_ratio >= MERGE_THRESHOLD;
            bbs_merged = bbs_filtered .* repmat(overlapping_ratio, 1, 5);
            bbs_merged(all(bbs_merged == 0, 2), :) = []; 
        else
            bbs_merged = []; 
        end
        
        % save the results
        I_before_resize = imread(fullfile(input_video_path, input_frames(input_id).name));
        save_image(I_before_resize, bbs_merged, MODE, detector_name, video, input_frames(input_id).name, 0);
        save_BB(bbs_merged, MODE, detector_name, video, filename);
    end
end