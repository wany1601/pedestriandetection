function pedestrian_detection_original(detector_id, video)
%%%%%
% Author: Yi Wang

% Pedestiran detection on original images. This function will save bounding
% boxes and output image for each frame in the video.

% input:
%   detector_id, int:
%   the detecter id. 1. ACF
%                    2. HOG+SVM
%   video, string:
%   the video to detect
%%%%%
    % constant
    NMS_THRESHOLD = 0.5;                        % the threshold for NMS
    PROJECT_PATH = strrep(pwd, 'code', '');     % project path
    MODE = 'filtered';      % the operation mode 'original', 'filtered', 'merged'
    
    % load the pre-calculated parameter: resize ratio
    load(fullfile(PROJECT_PATH, 'parameters', 'resize_ratio_all'));
   
    % each line in the file represent a detector, while each column
    % represent a video
    if str2double(video) > size(resize_ratio_all, 2)
        resize_ratio_video = 1;
    else
        resize_ratio_video = resize_ratio_all(detector_id, str2double(video));
    end
    
    input_video_path = fullfile(PROJECT_PATH, 'dataset', 'input', video);
    input_frames = dir(fullfile(input_video_path, '*jpg'));
    
    switch detector_id
        case 1
            detector_name = 'ACF';
            load('AcfCaltechDetector.mat'); %select between: 1. AcfInriaDetector 2. AcfCaltechDetector
            detector.opts.cascThr = -5;     %score threshold

            for input_id = 1 : length(input_frames)
                filename = input_frames(input_id).name;
                I_before_resize = imread(fullfile(input_video_path, filename));
                I_after_resize = imresize(I_before_resize, resize_ratio_video, 'nearest');
                
                % detection
                bbs_after_resize = acfDetect(I_after_resize, detector); 
                
                % resize back the bbs [x, y , h, w], keep the score.
                bbs_before_resize(:, 1 : 4) = round(bbs_after_resize(:, 1 : 4) / resize_ratio_video); 
                bbs_before_resize(:, 5) = bbs_after_resize(:, 5);
                
                % non maximum suppression
                bbs_before_resize = bbNms(bbs_before_resize, 'type','max', 'overlap', NMS_THRESHOLD);

                % save the results
                save_image(I_before_resize, bbs_before_resize, MODE, detector_name, video, filename, 0);
                save_BB(bbs_before_resize, MODE, detector_name, video, filename);
                clear bbs_before_resize
            end
            
        case 2
            detector_name = 'HOG+SVM';
            EXE_PATH = ['"', PROJECT_PATH, 'code\pedestrianDetector\HOG+SVM\HOG+SVM.exe"'];
            BB_path = fullfile(PROJECT_PATH, 'output', 'BB', filesep);
            BB_video_path = fullfile(BB_path, MODE, detector_name, video);
            if ~exist(BB_video_path, 'dir'), mkdir(BB_video_path); end
            output_image_path = fullfile(PROJECT_PATH, 'output', 'image', filesep);
            output_image_video_path = fullfile(output_image_path, MODE, detector_name, video);
            if ~exist(output_image_video_path, 'dir'), mkdir(output_image_video_path); end

            for input_id = 1 : length(input_frames)
                filename = input_frames(input_id).name;
                I_before_resize_path = fullfile(input_video_path, filename);
                
                % call the opencv HOG+SVM pedestrian detector
                system([EXE_PATH ' ' I_before_resize_path ' ' num2str(resize_ratio_video) ' ' MODE ' ' video ' ' filename ' ' BB_path ' ' output_image_path]);
                
                % read the bounding boxes and apply NMS
                BB_before_NMS_path = fullfile(BB_video_path, [filename, '.txt']);
                bbs_before_resize = load(BB_before_NMS_path);
                bbs_before_resize = bbNms(bbs_before_resize, 'type','max', 'overlap', NMS_THRESHOLD);
                I_before_resize = imread(I_before_resize_path);
                
                % save the results
                save_image(I_before_resize, bbs_before_resize, MODE, detector_name, video, filename, 0);
                save_BB(bbs_before_resize, MODE, detector_name, video, filename);
            end
    end
end

            
            