function save_BB(bbs, orig_or_filt, detector, video, file_name)
%%%%%
% Author: Yi Wang

% Save the output bounding boxes
% This function will make a folder to keep a .txt file for each frame
% at ../output/BB/orig_or_filt/detector/video

% input:
%   bbs, metrix:
%   the bounding boxs, [x, y, w, h, score]

%   orig_or_filt, string:
%   the type of the result, indicates the result is generated on 'orignial'
%   image, 'filtered' image or create the 'merged' result

%   detector, string:
%   detector name

%   video, string:
%   the video name

%   file_name, string:
%   the output file name
%%%%%

    % create the output path
    BB_path = fullfile('..', 'output', 'BB', orig_or_filt, [detector, '_NMS'], video);
    if ~exist(BB_path,'dir'), mkdir(BB_path); end
    
    % write the .txt file
    file_name = strrep(file_name, 'in', 'bb');
    file_name = strrep(file_name, 'jpg', 'txt');
    fid = fopen(fullfile(BB_path, file_name), 'w');
    if ~isempty(bbs)
        for i = 1 : size(bbs, 1)
            for j = 1 : size(bbs, 2) - 1
                fprintf(fid, '%d ', round(bbs(i, j)));
            end 
            fprintf(fid, '%0.1f\r\n', bbs(i, size(bbs, 2)));
        end
    end
    fclose(fid);
end