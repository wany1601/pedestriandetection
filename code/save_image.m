function save_image(I, bbs, orig_or_filt, detector, video, file_name, score_flag)
%%%%%
% Author: Yi Wang

% Save the output image
% This function will generate a folder to keep a .jpg file for each frame 
% at ../output/BB/orig_or_filt/detector/video

% input:
%   I, metrix:
%   the input image

%   bbs, metrix:
%   the bounding boxs, [x, y, w, h, score]

%   orig_or_filt, string:
%   the type of the result, indicate the result is generated on 'orignial'
%   image, 'filtered' image or create the 'merged' result.

%   detector, string:
%   detector name

%   video, string:
%   the video name

%   file_name, string:
%   the output file name

%   score_flag, bool:
%   whether add score for each bounding box while saving the image
%%%%%

    % create the output path
    save_path = fullfile('..', 'output', 'image', orig_or_filt, [detector, '_NMS'], video);
    if ~exist(save_path, 'dir'), mkdir(save_path); end

    if ~isempty(bbs)
        if score_flag == 0
            J = bbApply( 'embed', I, bbs, 'col',[0 255 0],'lw',2,'fh',0, 'fcol',[255,0,0]);
        else
            J = bbApply( 'embed', I, bbs, 'col',[0 255 0],'lw',2,'fh',size(I, 1)/ 30, 'fcol',[255,0,0]);
        end
    else
        J = I;
    end
    
    file_name = strrep(file_name, 'in', 'out');
    imwrite(J, fullfile(save_path, file_name), 'JPEG');
end

